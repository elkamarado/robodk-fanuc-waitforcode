# WaitForCode plug-in for RoboDK

## What is this?

This RoboDK add-on enables wait-for-a-code-and-select-a-program instructions for Fanuc robots.

The idea is generally to have a list of pairs of codes and programs to run.
Structurally the plug-in must have at least the next scripts:
 + GUI to change the list
 + a script to add a new program constisting a custom instruction
 + a script to build/simulate a program

The custom code must define a cycle designated to wait for a code in some register and to treat that code by
calling the corresponding program.

## Installation

 1. RoboDK must be installed
 2. Download the contents of this repository into the Apps folder inside of RoboDK file tree (example: C:\RoboDK\Apps)
 3. Now you should have something like (example: C:\RoboDK\Apps\robodk-fanuc-waitforcode)
 5. Open RoboDK and proceed to the main menu
 6. Select Tools -> Plug-ins
 7. In an opened window check if the AppLoader plug-in is on the list
 8. If the plugin is on the list already then double click it (red mark shown) and then double click it again (green mark shown)
 9. If not then click the Load Plug-in button and open AppLoader.dll
 10. The add-on is ready!

## Usage

The add-on actions located in the Wait For Code item in the main menu as well as in the toolbox.

First you want to activate the Code List item to set some code-program pairs. Behind the scenes the add-on stores it in the station parameter CodePairList in the form of JSONyfied Python dictionary. Please **DO NOT EDIT that parameter by hand** if you aren't confident enough.
Then call Add Wait Code to add a special program to the station.