# For more information about the RoboDK API:
# Documentation: https://robodk.com/doc/en/RoboDK-API.html
# Reference:     https://robodk.com/doc/en/PythonAPI/index.html
#-------------------------------------------------------
from robodk import *
from robolink import *
import tkinter as tk
import tkinter.ttk as ttk
import json

# Start the RoboDK API
RDK = Robolink()
class CodeListWindow(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.parent.title('Edit Code List')
        self.parent.geometry('500x420') 

        frm = self.parent
        
        regFrm = tk.Frame(master=frm, bd=1, relief=tk.SOLID)
        regFrm.pack(fill=tk.X, side=tk.TOP, padx=4, pady=4, ipady=4)
        rLabel = tk.Label(regFrm, text='Register R[i]', width=10)
        rLabel.pack(side=tk.LEFT)
        rEntry = tk.Entry(regFrm, show=None, font=('Arial', 14), width=4)
        rEntry.pack(side=tk.LEFT)
        giLabel = tk.Label(regFrm, text='  Group Input GI[i]', width=14)
        giLabel.pack(side=tk.LEFT)
        giEntry = tk.Entry(regFrm, show=None, font=('Arial', 14), width=4)
        giEntry.pack(side=tk.LEFT)

        topFrm = tk.Frame(master=frm, bd=1, relief=tk.SOLID)
        topFrm.pack(fill=tk.X, padx=4, pady=4, ipady=4)
        newItemFrm = tk.Frame(master=topFrm)
        newItemFrm.pack(side=tk.LEFT)

        newCodeFrm = tk.Frame(master=newItemFrm)
        newCodeFrm.pack(side=tk.LEFT)
        newCodeLabel = tk.Label(newCodeFrm, text='Code', width=6)
        newCodeLabel.pack(side=tk.LEFT)
        newCodeEntry = tk.Entry(newCodeFrm, show=None, font=('Arial', 14), width=4)
        newCodeEntry.pack(side=tk.RIGHT)
        newCodeEntry.focus_set()

        newProgFrm = tk.Frame(master=newItemFrm)
        newProgFrm.pack(side=tk.BOTTOM)
        newProgLabel = tk.Label(newProgFrm, text='Program', width=8)
        newProgLabel.pack(side=tk.LEFT)
        progSelectBtn = tk.Button(newProgFrm, width=6, text='Select...', command=self.select_program)
        progSelectBtn.pack(side=tk.RIGHT)
        progSelectBtn.bind('<KeyPress-Return>', func= lambda self: newPairBtn.focus_set())
        newProgEntry = tk.Entry(newProgFrm, show=None, font=('Arial', 14), width=12)
        newProgEntry.pack(side=tk.RIGHT)
        newProgEntry.bind('<KeyPress-Return>', func= lambda self: newPairBtn.focus_set())
        newCodeEntry.bind('<KeyPress-Return>', func= lambda self: progSelectionBtn.focus_set())

        topBtnFrm = tk.Frame(master=topFrm)
        topBtnFrm.pack(side=tk.RIGHT)
        newPairBtn = tk.Button(topBtnFrm, width=12, text='Add code', command=self.insert_pair)
        newPairBtn.pack()
        newPairBtn.bind('<KeyPress-Return>', func= lambda self: newCodeEntry.focus_set())
        delPairBtn = tk.Button(topBtnFrm, width=12, text='Delete Selected', command=self.delete_pair)
        delPairBtn.pack()
        clrPairBtn = tk.Button(topBtnFrm, width=12, text='Clear All', command=self.clear)
        clrPairBtn.pack()
        
        columns = ("Code", "Program")
        tree = ttk.Treeview(self.parent, columns=columns)
        tree.heading('Code', text='Code')
        tree.heading('Program', text='Program')
        tree.heading('#0', text='No')
        tree.column("#0", width=5)
        tree.pack(fill=tk.X, pady=4, padx=4)
        tree.bind('<KeyPress-Delete>', func=self.delete_pair)

        # treeScroll = tk.Scrollbar(tree)
        # treeScroll.pack(fill=tk.Y, side=tk.RIGHT)
        # treeScroll.configure(command=tree.yview)
        # tree.configure(yscrollcommand=treeScroll.set)
        
        footerFrm = tk.Frame(frm)
        footerFrm.pack(fill=tk.X, side=tk.BOTTOM, pady=4, padx=4)

        btn_Save = tk.Button(footerFrm, width=8, text='Save')
        btn_Save['command'] = self.save_changes
        btn_Gen = tk.Button(footerFrm, text='Generate a program')
        btn_Gen['command'] = self.generate
        btn_Cancel = tk.Button(footerFrm, width=8, text='Cancel')
        btn_Cancel['command'] = self.close
        btn_Cancel.pack(side='right')
        # btn_Gen.pack(side='right')
        btn_Save.pack(side='right')
        # the enter button will trigger the focused button's action
        btn_Save.bind('<KeyPress-Return>', func=self.save_changes)
        btn_Cancel.bind('<KeyPress-Return>', func=self.close)

        try:
            from robolink import getPathIcon
            iconpath = getPathIcon()
            self.parent.iconbitmap(iconpath)  
        except:
            print("RoboDK's Robolink module not found")
        
        self.parent.attributes("-topmost", True)           

        self.treeview = tree
        self.newcode = newCodeEntry
        self.newprog = newProgEntry
        self.reg = rEntry
        self.gi = giEntry

        self.id = 1

        self.load_data()

        self.parent.mainloop()
        # the function pauses here until the mainloop is quit
        # window.destroy()
        return

    def generate(self):
        pass

    def insert_pair(self):

        code = self.newcode.get()
        prog = self.newprog.get()
        if (len(code) == 0) or (int(code) == 0):
            return
        if len(prog) == 0:
            return

        self.treeview.insert(parent='', index='end', iid=self.id, text=str(self.id), values=(self.newcode.get(), self.newprog.get() ))
        self.id = self.id + 1

    def delete_pair(self):
        row_id = int(self.treeview.focus())
        self.treeview.delete(row_id)

    def select_program(self):
        RDK.setSelection([])
        program = RDK.ItemUserPick('Select a program', ITEM_TYPE_PROGRAM)
        if program.Valid():
            self.newprog.delete(0)
            self.newprog.insert(0, program.Name())

    def load_data(self):
        reg = RDK.getParam('CodePairR') 
        if reg is None:
            self.reg.delete(0)
            self.reg.insert(0,'100')
        else:
            self.reg.delete(0)
            self.reg.insert(0,str(int(reg)))

        gi = RDK.getParam('CodePairGI') 
        if gi is None:
            self.gi.delete(0)
            self.gi.insert(0,'1')
        else:
            self.gi.delete(0)
            self.gi.insert(0,str(int(gi)))

        param = RDK.getParam('CodePairList')
        if param is None:
            return
        codePairs = json.loads(param)
        
        last_iid = 0
        for key, value in codePairs.items():
            last_iid = int(key)
            # RDK.ShowMessage(str(last_iid))
            self.treeview.insert(parent='', index='end', iid=last_iid, text=key, values=(value[0], value[1] ))
        self.id = last_iid + 1

    def clear(self):
        self.treeview.delete(*self.treeview.get_children())

    def save_changes(self):
        RDK.setParam('CodePairR', self.reg.get()) 
        RDK.setParam('CodePairGI', self.gi.get()) 

        codePairs = dict()
        for line in self.treeview.get_children():
            item = self.treeview.item(line)
            codePairs[item['text']] = item['values']
        param = json.dumps(codePairs)
        RDK.setParam('CodePairList', param)
        self.parent.destroy()

    def close(self):
        self.parent.destroy()

# class CodeListWindow(tk.Frame):
#     def __init__(self, parent):
#         tk.Frame.__init__(self)
#         self.parent = parent
#         self.parent.title('Edit Code List')
#         # self.pack(tk.BOTH, expand = 1)
#         self.parent.mainloop()

root = tk.Tk()
window = CodeListWindow(root)
# root.mainloop()
# codeListWindow = CodeListWindow()
