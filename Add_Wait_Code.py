# For more information about the RoboDK API:
# Documentation: https://robodk.com/doc/en/RoboDK-API.html
# Reference:     https://robodk.com/doc/en/PythonAPI/index.html
#-------------------------------------------------------
from robodk import *
from robolink import *
import json
import sys

# Start the RoboDK API
RDK = Robolink()

robots = RDK.ItemList(filter=ITEM_TYPE_ROBOT)
if len(robots) == 0:
    RDK.ShowMessage("There are no robots to exploitate")
    sys.exit(1)
elif len(robots) == 1:
    robot = RDK.Item('', ITEM_TYPE_ROBOT)    
else:
    robot = RDK.ItemUserPick('Pick a robot', ITEM_TYPE_ROBOT)

if not robot.Valid():
    RDK.ShowMessage("There are no robots to exploitate")
    sys.exit(1)

param = RDK.getParam('CodePairList')
if param is None:
    RDK.ShowMessage("No codes given. Please, proceed to the Main Menu -> Wait For Code -> Code List")
    sys.exit(2)

reg = RDK.getParam('CodePairR') 
if reg is None:
    RDK.ShowMessage("No R[i] register index given. Please, proceed to the Main Menu -> Wait For Code -> Code List")
    sys.exit(3)

gi = RDK.getParam('CodePairGI') 
if gi is None:
    RDK.ShowMessage("No GI[i] group input index given. Please, proceed to the Main Menu -> Wait For Code -> Code List")
    sys.exit(4)

codePairs = json.loads(param)

waitprog = RDK.AddProgram('WaitCode', robot)
waitprog.RunInstruction('Cycle start', INSTRUCTION_COMMENT)
waitprog.RunInstruction('LBL[1:waitcode]', INSTRUCTION_INSERT_CODE)
waitprog.RunInstruction('Read GroupInput', INSTRUCTION_COMMENT)
waitprog.RunInstruction('R['+str(int(reg))+':code] = GI['+str(int(gi))+']', INSTRUCTION_INSERT_CODE)
waitprog.RunInstruction('Treat the code', INSTRUCTION_COMMENT)
waitprog.RunInstruction('SELECT R['+str(int(reg))+'] = 0, JMP LBL[1]', INSTRUCTION_INSERT_CODE)
for key, val in codePairs.items():
    code = str(val[0])
    prog = str(val[1])
    instr = '              = ' + code + ', CALL ' + prog
    waitprog.RunInstruction(instr, INSTRUCTION_INSERT_CODE)
waitprog.RunInstruction('              ELSE, JMP LBL[1]', INSTRUCTION_INSERT_CODE)

waitprog.setRobot(robot)
